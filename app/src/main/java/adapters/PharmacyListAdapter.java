package adapters;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.medfinder.R;

import java.util.ArrayList;

import customcontrols.CustomCircleImageView;
import customcontrols.CustomTextView;
import listeners.OnCapturePrescriptionClickListener;
import listeners.OnNavigationClickListener;
import listeners.PharmacyDetailsListener;
import utilities.CommonUtils;
import webservices.searchpharmacy.PharmacyList;

/**
 * Created by navdeep on 31/05/16.
 */
public class PharmacyListAdapter extends BaseAdapter {


    private final Context context;
    private ArrayList<PharmacyList> pharmacyList = new ArrayList<>();
    private PharmacyDetailsListener pharmacyDetailsListener;

    public PharmacyListAdapter(Context context) {
        this.context = context;
    }

    public PharmacyListAdapter(Context context, ArrayList<PharmacyList> pharmacyList, OnNavigationClickListener navigationClickListener, OnCapturePrescriptionClickListener onCapturePrescriptionClickListener, PharmacyDetailsListener PharmacyDetailsListener, boolean isPartialOrder) {
        this.context = context;
        this.pharmacyList = pharmacyList;
        this.pharmacyDetailsListener = PharmacyDetailsListener;
    }

    @Override
    public int getCount() {
        if (pharmacyList == null) {
            return 0;
        } else {
            return pharmacyList.size();
        }
    }

    @Override
    public Object getItem(int i) {
        if (pharmacyList != null) {
            return pharmacyList.get(i);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (view == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.pharmacy_list, viewGroup, false);
            holder.pharmacyNameTv = (CustomTextView) view.findViewById(R.id.pharmacyNameTV);
            holder.pharmacyAreaTv = (CustomTextView) view.findViewById(R.id.pharmacyAreaTV);
            holder.pharmacyMilesTV = (CustomTextView) view.findViewById(R.id.pharmacyMilesTV);
            holder.pharmacyLogo = (CustomCircleImageView) view.findViewById(R.id.pharmacyLogo);

            holder.cardView = (CardView) view.findViewById(R.id.cardView);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.pharmacyNameTv.setBold(true);
        holder.pharmacyNameTv.setTextColor(context, R.color.colorPrimary);

        holder.pharmacyNameTv.setText(pharmacyList.get(position).getPharmacyName());
        holder.pharmacyNameTv.setContentDescription(pharmacyList.get(position).getPharmacyName());

        holder.pharmacyAreaTv.setText(pharmacyList.get(position).getAreaName());
        holder.pharmacyAreaTv.setContentDescription(pharmacyList.get(position).getAreaName());

        if (pharmacyList.get(position).getPharmacyLogo() == null || pharmacyList.get(position).getPharmacyLogo().isEmpty()) {
            holder.pharmacyLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.pharmacylogo));

        } else {
            Bitmap pharmacyLogo = CommonUtils.base64ToBitmap(pharmacyList.get(position).getPharmacyLogo());
            holder.pharmacyLogo.setImageBitmap(pharmacyLogo);
        }

        if (pharmacyList.get(position).getDistance() != null && (!pharmacyList.get(position).getDistance().isEmpty())) {
            holder.pharmacyMilesTV.setText(pharmacyList.get(position).getDistance());
            holder.pharmacyMilesTV.setContentDescription(pharmacyList.get(position).getDistance());

            view.setContentDescription(pharmacyList.get(position).getPharmacyName() + "" + pharmacyList.get(position).getAreaName() + "" + pharmacyList.get(position).getDistance());
            holder.pharmacyMilesTV.setVisibility(View.VISIBLE);
        } else {
            holder.pharmacyMilesTV.setVisibility(View.INVISIBLE);
            view.setContentDescription(pharmacyList.get(position).getPharmacyName() + "" + pharmacyList.get(position).getAreaName());

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            StateListAnimator stateListAnimator = AnimatorInflater
                    .loadStateListAnimator(context, R.animator.lift_on_touch);
            holder.cardView.setStateListAnimator(stateListAnimator);
        }

        holder.cardView.setOnClickListener(new getPharmacyDetails(pharmacyList.get(position).getPharmacyId()));

        return view;
    }

    /**
     * Static class
     */
    static class ViewHolder {
        CustomTextView pharmacyNameTv, pharmacyAreaTv, pharmacyMilesTV;
        CardView cardView;
        CustomCircleImageView pharmacyLogo;

    }

    public class getPharmacyDetails implements View.OnClickListener {

        private final String pharmacyID;

        public getPharmacyDetails(String pharmacyID) {
            this.pharmacyID = pharmacyID;
        }

        @Override
        public void onClick(View v) {
            pharmacyDetailsListener.getPharmacyDetails(pharmacyID);
        }
    }

}